import React, { useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import Cell from "./Cell";
import "./main.css";

function TaskList() {
  const [cols, setCols] = useState([
    ["Column ", ""],
    ["Column ", ""],
    ["Column ", ""],
  ]);
  // const [isEdit, setIsEdit] = useState(false);
  // const [buffer1, setBuffer1] = useState({});
  // const [buffer2, setBuffer2] = useState({});
  // const [enterTarget, setEnterTarget] = useState(null);
  // const [depth, setDepth] = useState(0);

  // const [isDragging, setIsDragging] = useState(false);
  const [divEditable, setDivEditable] = useState(false);

  const onChange = (e, i, j) => {
    let arr = cols;
    arr[i][j] = e.target.value;
    setCols([...arr]);
  };
  const addCell = (e, i) => {
    const arr = cols;
    arr[i].push("");
    setCols([...arr]);
  };
  const addCol = () => {
    const arr = cols;
    arr.push([`Column `, ""]);
    setCols([...arr]);
  };

  const removeCol = e =>{
    const arr = cols ;
    arr.splice(e.target.id,1)
    setCols([...arr])
  }

  const reorder = (col_id, startIndex, endIndex) => {
    const result = cols;
    const [removed] = result[col_id].splice(startIndex, 1);
    result[col_id].splice(endIndex, 0, removed);


    setCols(result)
  };


  const move = (source, destination) => {
    const result = cols;
    const [removed] = result[parseInt(source.droppableId)].splice(source.index, 1);
    result[parseInt(destination.droppableId)].splice(destination.index, 0, removed);
    setCols(result)
  };

  //
  const onDragEnd = result => {
    const { source, destination } = result;

    if (!destination) {
      return;
    }
    if (destination.index === 0) {
      const result = cols;
      const [removed] = result[parseInt(source.droppableId)].splice(source.index, 1);
      result[parseInt(destination.droppableId)].splice(1, 0, removed)
      setCols(result)
      return
    }
    if (source.droppableId === destination.droppableId) {
      reorder(
        parseInt(source.droppableId),
        parseInt(source.index),
        parseInt(destination.index)
      )
    } else {
      move(source, destination)
    }
  }


  const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: "none",
    background: isDragging ? "rgba(194, 201, 250, 0.4)" : "white",
    borderRadius: "5px",
    boxShadow: "1px 3px 1px rgba(0,0,0,.2) ",
    margin: "10px",

    // styles we need to apply on draggables
    ...draggableStyle,
  });

  const getListStyle = (isDraggingOver) => ({
    background: isDraggingOver ? "rgb(238, 238, 238)" : "rgb(235, 236, 240)",
    width: 220,
    paddingBottom : 20
  });

  return (
    <div className="taskListWrapper" style={{ width: "1000px" }}>
      <hr />
      <h3>TaskList</h3>
      <div>
       
      </div>
      <div className="taskList">
        <DragDropContext onDragEnd={onDragEnd}>
          {cols.map((col, i) => {
            return (
              <Droppable droppableId={i.toString()}>
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                    {...provided.droppableProps}
                  >
                    {col.map((el, j) => {
                      if (j === 0) {
                        return (
                          <div
                            className="colHeaderWrapper"
                            key={j}>
                            <Cell
                              id={j}
                              key={j}
                              // onChange={(e) => onChange(e, i, j)}
                              // isEdit={isEdit}
                              value={`${el} ${i+1}`}
                            />
                            <button id ={i} onClick={removeCol}>
                              x
                            </button>
                          </div>
                        )

                      }
                      return (
                        <Draggable
                          key={j}
                          draggableId={`cell_${i}_${j}`}
                          index={j}
                        >
                          {(provided, snapshot) => (
                            <div
                              className="cellWrapper"
                              key={j}
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={getItemStyle(
                                snapshot.isDragging,
                                provided.draggableProps.style
                              )}
                              onClick={e => {
                                console.log("clicked")
                                return setDivEditable(true)
                              }}
                            >
                              <Cell
                                key={j}
                                id={j}
                                onBlur={e => setDivEditable(false)}
                                onChange={(e) => onChange(e, i, j)}
                                // isEdit={isEdit}
                                value={el}
                                divEditable={divEditable}
                              />
                            </div>
                          )}
                        </Draggable>

                      );
                    })}
                    {provided.placeholder}
                    <div className="buttonWrapper">

                      <button
                        className="addButton"
                        onClick={(e) => addCell(e, i)}
                      >
                        Add Cell+
                      </button>
                    </div>
                  </div>
                )}
              </Droppable>
            );
          })}
        </DragDropContext>

        <span>
          <button className="addColButton" onClick={(e) => addCol(e)}>
            Add Col+
          </button>
        </span>
      </div>
    </div>
  );
}

export default TaskList;
