import React, { useState } from "react";
// import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import "./table.css";

function Table() {
  const [cols, setCols] = useState([
    ["Column ", "","",""],
    ["Column ", "","",""],
    ["Column ", "","",""],
  ]);

  const onChangeHandler = (e, i, j) => {
    const arr = cols;
    cols[i][j] = e.target.value;
    setCols([...arr]);
  };

  const addRow = (e) => {
    const arr = cols;
    arr.forEach((col) => {
      col.push("");
    });

    setCols([...arr]);
  };

  const addCol = (e) => {
    // const arr = cols ;
    const arr= ["Column "];
    const n = cols[0].length ;
    
    for(let i=1;i<n;i++){
      arr.push("")
    }
    setCols([...cols,arr])
  };

  const removeCol = (e) => {
    // console.log(e.target.id)
    const arr = cols;
    arr.splice(e.target.id, 1);
    // console.log(arr)
    setCols([...arr]);
  };

  const removeRow=(e,j)=>{
    const arr = cols ;
    arr.forEach(col=>{
      col.splice(j,1)
    })
    setCols([...arr])
  
  }

  return (
    <div className="wrapper">
      <h3>Table</h3>
      <div className="tableWrapper">
        <div className="table">
          <div></div>
          {cols.map((col, i) => {
            return (
              <div id={i} className="table_col">
                {col.map((el, j) => {
                  if (!j) {
                    return (
                      <div className="colHeaderWrapper">
                        <input
                          className="tableColHeader"
                          id={j}
                          placeHolder="Edit"
                          onChange={(e) => onChangeHandler(e, i, j)}
                          value={el === "Column " ? `${el}${i + 1}` : el}
                        ></input>
                        <button id={i} onClick={removeCol}>
                          x
                        </button>
                      </div>
                    );
                  }
                  return (
                    <div className="table_cell">
                      <input
                        className="table_input"
                        id={j}
                        placeHolder="Edit"
                        onChange={(e) => onChangeHandler(e, i, j)}
                        value={el}
                      />
                      {i === cols.length-1 ? (
                        <button id={i} onClick={e=>removeRow(e,j)}>
                        x
                      </button>
                      ):null }
                    </div>
                  );
                })}
              </div>
            );
          })}
          <button className="tableAddCol" onClick={(e) => addCol(e)}>
            Add Col +{" "}
          </button>
        </div>
        <button className="tableAddRow" onClick={(e) => addRow(e)}>
          Add Row +{" "}
        </button>
      </div>
    </div>
  );
}

export default Table;
