import React, { useState } from "react";
import "./Cell.css";

function Cell(props) {
  // const [value, setValue] = useState();
  // const [buffer, setBuffer] = useState("");
  const onChangeHandler = (e) => {
    // props.isEdit && 
    props.onChange(e);
  };

  return (
  
    props.divEditable ? (
    <div className="cell">

    <input
      className="input"
      id={props.id}
      onChange={onChangeHandler}
        value={props.value}
      onBlur={props.onBlur}
      // autofocus ="true"
      ></input>
      </div>
      ) :
      (<p className="cell">
        {props.value}
      </p>)



    // !props.isEdit ?   
    //   (<div className="cell">
    //   {props.value}
    //   </div> )
    //   :(<input
    //   className="input"
    //   id={props.id}
    //   placeHolder={props.value}
    //   onChange={onChangeHandler}
    //   value={props.value}
    // ></input> )


  );
}

export default Cell;
